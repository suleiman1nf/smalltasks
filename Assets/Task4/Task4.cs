namespace Task4.Scripts
{
    public struct ApiSetup<T>
    {
    }

    public static class ExtensionMethods
    {
        public static void SetupObjectA<T>(this ApiSetup<T> obj) where T : ISomeInterfaceA
        {
            
        }
        public static void SetupObjectB<T>(this ApiSetup<T> obj) where T : ISomeInterfaceB
        {
            
        }
    }
    class Api
    {
        public ApiSetup<T> For<T>(T obj)
        {
            return new ApiSetup<T>();
        }
    }

    public interface ISomeInterfaceA
    { }

    public interface ISomeInterfaceB
    { }
    public class ObjectA : ISomeInterfaceA
    { }
    public class ObjectB : ISomeInterfaceB
    { }

    class SomeClass
    {
        public void Setup()
        {
            Api apiObject = new Api();
            
            apiObject.For(new ObjectA()).SetupObjectA();
            // apiObject.For(new ObjectB()).SetupObjectA(); // wrong
            // apiObject.For(new ObjectA()).SetupObjectB(); // wrong
            apiObject.For(new ObjectB()).SetupObjectB();
        }
    }
}
