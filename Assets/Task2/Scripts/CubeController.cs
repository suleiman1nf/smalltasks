using UnityEngine;

namespace Task2.Scripts
{
    public class CubeController : MonoBehaviour
    {
        private float speed;
        private float deathDistance;
        private Vector3 startPoint;

        public void Init(Vector3 startPoint, Vector3 startAngle, float speed, float deathDistance)
        {
            this.startPoint = startPoint;
            this.deathDistance = deathDistance;
            this.speed = speed;

            transform.localEulerAngles = startAngle;
            transform.localPosition = startPoint;
        }

        private bool IsAlive()
        {
            return Vector3.Distance(startPoint, transform.position) < deathDistance;
        }

        void Update()
        {
            if (IsAlive())
            {
                transform.Translate(transform.forward * (speed * Time.deltaTime));
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
