using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Task2.Scripts
{
    public class GameManager : MonoBehaviour
    {
        [Header("UI")]
        [SerializeField] private TMP_InputField inputTime;
        [SerializeField] private TMP_InputField inputDistance;
        [SerializeField] private TMP_InputField inputSpeed;
        [SerializeField] private Button changeStateButton;
        [SerializeField] private TMP_Text changeStateText;
    
        [Space(20)] 
        [SerializeField] private Transform cubesParent;
        [SerializeField] private CubeController cubePrefab;

        private float createTime = 0.5f;
        private float speed = 0;
        private float distance = 0;
        private Coroutine cubesCoroutine;

        private bool isStop = true;

        public bool IsStop
        {
            get => isStop;
            set
            {
                isStop = value;
                if (!isStop)
                {
                    StartCubesSpawn();
                }
            }
        }

        private void Start()
        {
            changeStateButton.onClick.AddListener(OnChangeStateButtonClick);
            inputTime.onValueChanged.AddListener(SetTime);
            inputDistance.onValueChanged.AddListener(SetDistance);
            inputSpeed.onValueChanged.AddListener(SetSpeed);
        }

        private void OnDestroy()
        {
            changeStateButton.onClick.RemoveListener(OnChangeStateButtonClick);
            inputTime.onValueChanged.RemoveListener(SetTime);
            inputDistance.onValueChanged.RemoveListener(SetDistance);
            inputSpeed.onValueChanged.RemoveListener(SetSpeed);
        }

        private void CreateCube()
        {
            CubeController cubeController = Instantiate(cubePrefab);
            Vector3 startPos = new Vector3(Random.Range(0f, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
            Vector3 startAngle = new Vector3(0, Random.Range(0, 360), 0);
            cubeController.Init(startPos, startAngle, speed, distance);
            cubeController.transform.SetParent(cubesParent);
        }

        private void StartCubesSpawn()
        {
            if (cubesCoroutine != null)
                StopCoroutine(cubesCoroutine);
        
            cubesCoroutine = StartCoroutine(CreateCubeCoroutine(createTime));
        }

        private IEnumerator CreateCubeCoroutine(float delayTime)
        {
            CreateCube();
            yield return new WaitForSeconds(delayTime);
            if(!isStop)
                StartCubesSpawn();
        }

        private void SetTime(string text)
        {
            float.TryParse(text, out createTime);
        }
    
        private void SetSpeed(string text)
        {
            float.TryParse(inputSpeed.text, out speed);
        }
    
        private void SetDistance(string text)
        {
            float.TryParse(inputDistance.text, out distance);
        }

        private void OnChangeStateButtonClick()
        {
            IsStop = !isStop;
            changeStateText.text = IsStop ? "Start" : "Stop";
        }
    }
}
