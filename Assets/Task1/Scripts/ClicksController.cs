using UnityEngine;

namespace Task1.Scripts
{
    public class ClicksController : MonoBehaviour
    {
        private Clicks clicks;

        public void SetClicksModel(Clicks clicks)
        {
            this.clicks = clicks;
        }

        public void OnButtonClick()
        {
            clicks.AddClick();
        }
    }
}
