using System;

namespace Task1.Scripts
{
    public class Clicks
    {
        public event EventHandler<ClicksArgs> ONClicksUpdate;
        private int _clicksCount = 0;
        public int ClicksCount
        {
            get => _clicksCount;
            private set
            {
                _clicksCount = value;
                ONClicksUpdate?.Invoke(this, new ClicksArgs(){clicksCount = value});
            }
        }

        public void AddClick()
        {
            ClicksCount++;
        }
    
    }

    public class ClicksArgs : EventArgs
    {
        public int clicksCount;
    }
}