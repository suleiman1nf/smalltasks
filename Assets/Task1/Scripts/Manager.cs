using UnityEngine;

namespace Task1.Scripts
{
    public class Manager : MonoBehaviour
    {
        private Clicks clicks;
        [SerializeField] private ClicksController clicksController;
        [SerializeField] private ClicksView clicksView;

        private void Start()
        {
            clicks = new Clicks();
            clicksController.SetClicksModel(clicks);
            clicksView.SetClicksModel(clicks);
        }
    }
}
