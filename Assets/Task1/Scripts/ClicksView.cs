using TMPro;
using UnityEngine;

namespace Task1.Scripts
{
    public class ClicksView : MonoBehaviour
    {
        [SerializeField] private TMP_Text currentClicksText;
        private Clicks clicks;
    
        public void SetClicksModel(Clicks clicks)
        {
            this.clicks = clicks;
            clicks.ONClicksUpdate += ONClicksUpdate;
            ONClicksUpdate(this, new ClicksArgs(){clicksCount = clicks.ClicksCount});
        }

        private void ONClicksUpdate(object sender, ClicksArgs e)
        {
            UpdateText(e.clicksCount);
        }

        private void UpdateText(int clicksCount)
        {
            currentClicksText.text = $"Количество кликов: {clicksCount}";
        }

        private void OnDestroy()
        {
            clicks.ONClicksUpdate -= ONClicksUpdate;
        }
    }
}
